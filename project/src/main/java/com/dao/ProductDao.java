package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import com.model.Product;

@Service
public class ProductDao {
	@Autowired
	ProductRepository productRepository;

	@GetMapping
	public List<Product> getProducts() {
		return productRepository.findAll();
	}

	public Product getProductById(Long pId) {
		return productRepository.findById(pId).orElse(null);
	}

	public List<Product> getProductByNurseryId(int nursery_id) {
	    return productRepository.findByNurseryId(nursery_id);
	}


	public Product getProductByName(String productName) {
		return productRepository.findByProductName(productName);
	}

	public Product getProductByPrice(Long price) {
		return productRepository.findByPrice(price);
	}

	public List<Product> getProductByPlantCategory(String plant) {
		return productRepository.findByPlantCategory(plant);
	}

	public List<Product> getProductByPotCategory(String pot) {
		return productRepository.findByPotCategory(pot);
	}

	public List<Product> getProductBySoilCategory(String soil) {
		return productRepository.findBySoilCategory(soil);
	}

	public List<Product> getProductBySeedCategory(String seed) {
		return productRepository.findBySeedCategory(seed);
	}

	public List<Product> getProductByFertilizerCategory(String fertilizer) {
		return productRepository.findByFertilizerCategory(fertilizer);
	}

	public List<Product> getProductByToolCategory(String tool) {
		return productRepository.findByToolCategory(tool);
	}

	public List<Product> getProductByPesticideCategory(String pesticide) {
		return productRepository.findByPesticideCategory(pesticide);
	}

	public Product getProductBySpecifications(String specifications) {
		return productRepository.findBySpecifications(specifications);
	}

	public Product addProduct(Product product) {
		return productRepository.save(product);
	}

	public Product updateProduct(Product product) {
		return productRepository.save(product);
	}

	public void deleteProductById(Long pId) {
		productRepository.deleteById(pId);
	}

}
