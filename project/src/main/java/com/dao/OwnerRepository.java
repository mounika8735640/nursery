package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Owner;

@Repository
public interface OwnerRepository extends JpaRepository<Owner, Long> {

	@Query("from Owner where name = :name")
	Owner findByName(@Param("name") String name);

	@Query("from Owner where email_Id = :email_Id and password = :password")
	Owner ownerLogin(@Param("email_Id") String email_Id, @Param("password") String password);

	@Query(" from Owner where email_Id = :email_Id")
	Owner findByEmailId(@Param("email_Id") String emailId);

	@Query(" from Owner where location = :location")
	Owner findByLocation(@Param("location") String location);


}