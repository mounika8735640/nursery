package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;

import com.model.CartItem;

@Repository
public class CartItemDao {

	@Autowired
	private CartItemRepository cartItemRepository;

	@GetMapping
	public List<CartItem> getAllCartItems() {
		return cartItemRepository.findAll();
	}

	public CartItem getCartItemById(Long cartItemId) {
		return cartItemRepository.findById(cartItemId).orElse(null);
	}

	public CartItem addCartItem(CartItem cartItem) {
		return cartItemRepository.save(cartItem);
	}

	public CartItem updateCartItem(CartItem cartItem) {
		return cartItemRepository.save(cartItem);
	}

	public void deleteCartItemById(Long cartItemId) {
		cartItemRepository.deleteById(cartItemId);
	}

}
