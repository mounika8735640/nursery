package com.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import com.model.Cart;

@Service
public class CartDao {

	@Autowired
	private CartRepository cartRepository;

	@GetMapping
	public List<Cart> getAllCarts() {
		return cartRepository.findAll();
	}

	public Cart getCartById(Long cart_id) {
		return cartRepository.findById(cart_id).orElse(null);
	}

	@PostMapping
	public Cart addCart(Cart cart) {
		return cartRepository.save(cart);
	}

	@PutMapping
	public Cart updateCart(Cart cart) {
		return cartRepository.save(cart);
	}

	@DeleteMapping
	public void deleteCartById(Long cart_id) {
		cartRepository.deleteById(cart_id);
	}

}
