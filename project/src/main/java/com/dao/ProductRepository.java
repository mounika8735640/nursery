package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

	@Query("from Product where productName =:productName")
	Product findByProductName(@Param("productName") String productName);

	@Query("from Product where price =:price")
	Product findByPrice(@Param("price") Long price);

	@Query("from Product where specifications =:specifications")
	Product findBySpecifications(@Param("specifications") String specifications);

	@Query("from Product where category =:plant")
	List<Product> findByPlantCategory(@Param("plant") String plant);

	@Query("from Product where category =:pot")
	List<Product> findByPotCategory(@Param("pot") String pot);

	@Query("from Product where category =:soil")
	List<Product> findBySoilCategory(@Param("soil") String soil);

	@Query("from Product where category =:fertilizer")
	List<Product> findByFertilizerCategory(@Param("fertilizer") String fertilizer);

	@Query("from Product where category =:pesticide")
	List<Product> findByPesticideCategory(@Param("pesticide") String pesticide);

	@Query("from Product where category =:tool")
	List<Product> findByToolCategory(@Param("tool") String tool);

	@Query("from Product where category =:seed")
	List<Product> findBySeedCategory(@Param("seed") String seed);

	@Query("from Product where nursery_id =:nursery_id")
	List<Product> findByNurseryId(@Param("nursery_id") int nursery_id);

}
