package com.dao;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.model.Owner;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

@Service
public class OwnerDao {

	@Autowired
	OwnerRepository ownerRepository;

	@Autowired
	private JavaMailSender mailSender;


	// Twilio credentials
	private static final String ACCOUNT_SID = "ACa83e3dbcd8ca6019893558f571b895be";
	private static final String AUTH_TOKEN = "1d27211ce3a06c01bbdf18704f443554";
	private static final String TWILIO_PHONE_NUMBER = "+16073036141";

	private String otp;

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	static {
		Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
	}

	@GetMapping
	public List<Owner> getOwners() {
		return ownerRepository.findAll();
	}

	@GetMapping

	public Owner getOwnerById(Long ownerId) {
		return ownerRepository.findById(ownerId).orElse(null);
	}

	@GetMapping
	public Owner getOwnerByName(String ownerName) {
		return ownerRepository.findByName(ownerName);
	}

	@GetMapping
	public Owner getOwnerByLocation(String location) {
		return ownerRepository.findByLocation(location);
	}

	@GetMapping
	public Owner ownerLogin(String email_Id, String password) {
		Owner owner = ownerRepository.findByEmailId(email_Id);


		if (owner != null) {
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			if (passwordEncoder.matches(password, owner.getPassword())) {
				return owner;
			}
		}

		return null;
	}

	@PostMapping
	public Owner addOwner(@RequestBody Owner owner) {
		BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
		String encryptedPwd = bcrypt.encode(owner.getPassword());
		owner.setPassword(encryptedPwd);
		Owner savedOwner = ownerRepository.save(owner);

		String otp = generateOtp();
		setOtp(otp);
		sendWelcomeEmail(savedOwner);
		sendOtpSMS(savedOwner);

		return savedOwner;

	}

	private String generateOtp() {
		// Generate a 6-digit OTP
		Random random = new Random();
		int otp = 100000 + random.nextInt(900000);
		return String.valueOf(otp);
	}

	private void sendWelcomeEmail(Owner owner) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(owner.getEmailId());
		message.setSubject("Welcome to our online nursery plant store");
		message.setText("Dear " + owner.getName() + ",\n\n"
				+ "Thank you for registering with our EverGreen Nursery! We're thrilled to have you join our community of plant enthusiasts.\n\n"
				+ "At EverGreen Nursery, we are committed to providing you with a wide variety of healthy and vibrant plants to beautify your home or office space. "
				+ "Whether you're an experienced gardener or just starting out, we have something for everyone.\n\n"
				+ "Feel free to explore our website and discover our extensive collection of plants, gardening accessories, and expert tips. "
				+ "If you have any questions or need assistance, our friendly team is here to help.\n\n"
				+ "Happy gardening!\n\n" + "Best regards,\n" + "EverGreen Nursery Team");
		message.setSubject("Welcome to our website");
		message.setText("Dear " + owner.getName() + ",\n\n" + "Thank you for registering ");

		mailSender.send(message);
	}

	private void sendOtpSMS(Owner owner) {
		Message message = Message.creator(new PhoneNumber(owner.getPhoneNumber()), new PhoneNumber(TWILIO_PHONE_NUMBER),
				"Your OTP for registration is: " + getOtp()).create();

		System.out.println("SMS Sent SID: " + message.getSid());
	}

	private void sendOtpEmail(Owner owner) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(owner.getEmailId());
		message.setSubject("Password Reset OTP");
		message.setText("Dear " + owner.getName() + ",\n\n" + "Your OTP for password reset is: " + owner.getOtp());

		mailSender.send(message);
	}

	public Owner getOwnerEmailId(String emailId) {
		Owner owner = ownerRepository.findByEmailId(emailId);
		if (owner != null) {
			String otp = generateOtp();
			owner.setOtp(otp);
			sendOtpEmail(owner);
		}
		return owner;
	}

	public Owner updateOwnerPassword(String emailId, String password) {
		Owner owner = ownerRepository.findByEmailId(emailId);
		Owner savedOwner = null;
		if (owner != null) {
			BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
			String encryptedPwd = bcrypt.encode(owner.getPassword());
			owner.setPassword(encryptedPwd);
			savedOwner = ownerRepository.save(owner);
		}
		return savedOwner;
	}

	@PutMapping
	public Owner updateOwner(Owner updatedOwner) {
		Owner existingOwner = ownerRepository.findById(updatedOwner.getOwnerId()).orElse(null);

		if (existingOwner != null) {
			existingOwner.setName(updatedOwner.getName());
			existingOwner.setEmailId(updatedOwner.getEmailId());
			existingOwner.setPhoneNumber(updatedOwner.getPhoneNumber());

			if (!updatedOwner.getPassword().equals(existingOwner.getPassword())) {
				BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
				String encryptedPassword = passwordEncoder.encode(updatedOwner.getPassword());
				existingOwner.setPassword(encryptedPassword);
			}

			ownerRepository.save(existingOwner);

			return existingOwner;
		}

		return null;
	}

	@DeleteMapping


	public void deleteOwnerById(Long ownerId) {
		ownerRepository.deleteById(ownerId);
	}

}