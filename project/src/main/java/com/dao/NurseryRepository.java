package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import com.model.Nursery;
import com.model.Owner;
import com.model.User;

@Repository
public interface NurseryRepository extends JpaRepository<Nursery, Integer> {

	@Query("from Nursery where nurseryName = :nurseryName")
	Nursery findNurseryByName(@Param("nurseryName") String nurseryName);

	@Query("from Nursery where emailId = :emailId and password = :password")
	Nursery nurseryLogin(@Param("emailId") String emailId, @Param("password") String password);

	@Query(" from Owner where emailId = :emailId")
	Nursery findByEmailId(@Param("emailId") String emailId);

}
