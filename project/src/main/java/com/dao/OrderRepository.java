package com.dao;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import com.model.Order;
import com.model.User;

public interface OrderRepository extends JpaRepository<Order, Long>{
	 List<Order> findByUser(User user);
}
