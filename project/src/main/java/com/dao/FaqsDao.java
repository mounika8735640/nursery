package com.dao;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.model.Faqs;

@Service

public class FaqsDao {
	@Autowired
	FaqsRepository faqsRepository;

	public List<Faqs> getFaqs() {
		return faqsRepository.findAll();
	}

	public Faqs getFaqsById(Long fId) {
		return faqsRepository.findById(fId).orElse(null);
	}

}
