package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class CartItem {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long cartItemId;

	@ManyToOne
	@JoinColumn(name = "pId")
	private Product product;
	private int quantity;
	private double priceforeachitem;
	private double totalPrice;
	
	@ManyToOne
    @JoinColumn(name = "cartId")
    private Cart cart;


	public CartItem() {
	}

	public CartItem(Long cartItemId, Product product, int quantity, double priceforeachitem, double totalPrice) {
	
		this.cartItemId = cartItemId;
		this.product = product;
		this.quantity = quantity;
		this.priceforeachitem = priceforeachitem;
		this.totalPrice = totalPrice;
	}

	public Long getCartItemId() {
		return cartItemId;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getPriceforeachitem() {
		return priceforeachitem;
	}

	public void setPriceforeachitem(double priceforeachitem) {
		this.priceforeachitem = priceforeachitem;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
	
}
