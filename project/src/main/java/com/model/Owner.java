package com.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Owner {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long ownerId;
	private String name;

	@Column(unique = true)
	private String emailId;
	private String password;
	private String phoneNumber;
	private String location;
	private String otp;
	@OneToMany(mappedBy = "owner")
    private List<Nursery> nurseries;
	private Owner() {

	}


	public Owner(String name, String emailId, String password, String phoneNumber,String loaction,String otp) {

		this.name = name;
		this.emailId = emailId;
		this.password = password;
		this.phoneNumber = phoneNumber;
		this.location = location;
		this.otp =otp;
		
	}

	public Long getOwnerId() {
		return ownerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

}