package com.model;



import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToOne;

@Entity

public class Pesticides  {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	private int pesticideId;
	private String pesticideName;
	private String types;
	private String specifications;
	private String description;
	@Lob
	private byte[] image;
	private Long price;
	
	 @OneToOne
	    private Product product;
	
	public Pesticides(){
		
	}

	public Pesticides(String pesticideName, String types, String specifications, String description,
			byte[] image, Long price) {
		
		this.pesticideName = pesticideName;
		this.types = types;
		this.specifications = specifications;
		this.description = description;
		this.image = image;
		this.price = price;
	}

	public Pesticides(int pesticideId, String pesticideName, String types, String specifications, String description,
			byte[] image, Long price) {
		super();
		this.pesticideId = pesticideId;
		this.pesticideName = pesticideName;
		this.types = types;
		this.specifications = specifications;
		this.description = description;
		this.image = image;
		this.price = price;
	}

	public int getPesticideId() {
		return pesticideId;
	}

	public String getPesticideName() {
		return pesticideName;
	}

	public void setPesticideName(String pesticideName) {
		this.pesticideName = pesticideName;
	}

	public String getTypes() {
		return types;
	}

	public void setTypes(String types) {
		this.types = types;
	}

	public String getSpecifications() {
		return specifications;
	}

	public void setSpecifications(String specifications) {
		this.specifications = specifications;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}
	
}
