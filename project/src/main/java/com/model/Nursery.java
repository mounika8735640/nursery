package com.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Nursery {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int nurseryId;
	private String nurseryName;
	private String location;
	private String phoneNumber;
	@Column(unique = true)
	private String emailId;
	private String password;
	private String otp;
	private String imageName;
	@ManyToOne
	@JoinColumn(name = "ownerId", nullable = false)
	private Owner owner;

	@OneToMany
	@JsonIgnore
	private List<Product> products = new ArrayList<Product>();

	public Nursery() {

	}

	public Nursery(int nurseryId, String nurseryName, String location, String phoneNumber, String emailId,
			String password, String otp, String imageName) {

		this.nurseryId = nurseryId;
		this.nurseryName = nurseryName;
		this.location = location;
		this.phoneNumber = phoneNumber;
		this.emailId = emailId;
		this.password = password;
		this.otp = otp;
		this.imageName = imageName;
	}

	public int getNurseryId() {
		return nurseryId;
	}

	public void setNurseryId(int nurseryId) {
		this.nurseryId = nurseryId;
	}

	public String getNurseryName() {
		return nurseryName;
	}

	public void setNurseryName(String nurseryName) {
		this.nurseryName = nurseryName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public Owner getOwner() {
		return owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	@Override
	public String toString() {
		return "Nursery [nurseryId=" + nurseryId + ", nurseryName=" + nurseryName + ", location=" + location
				+ ", phoneNumber=" + phoneNumber + ", emailId=" + emailId + ", password=" + password + ", otp=" + otp
				+ ", imageName=" + imageName + ", owner=" + owner + ", products=" + products + "]";
	}

}
/*
 * 
 * 
 * public Nursery() { }
 * 
 * public int getNurseryId() { return nurseryId; }
 * 
 * public String getNurseryName() { return nurseryName; }
 * 
 * public void setNurseryName(String nurseryName) { this.nurseryName =
 * nurseryName; }
 * 
 * public String getLocation() { return location; }
 * 
 * public void setLocation(String location) { this.location = location; }
 * 
 * public String getPhoneNumber() { return phoneNumber; }
 * 
 * public void setPhoneNumber(String phoneNumber) { this.phoneNumber =
 * phoneNumber; }
 * 
 * public String getEmailId() { return emailId; }
 * 
 * public void setEmailId(String emailId) { this.emailId = emailId; }
 * 
 * public String getPassword() { return password; }
 * 
 * public void setPassword(String password) { this.password = password; }
 * 
 * @JsonProperty("ownerId") public Long getOwnerId() { return (Long)
 * ((this.owner != null) ? this.owner.getOwnerId() : 0); }
 * 
 * public List<Product> getProducts() { return products; }
 * 
 * public void setProducts(List<Product> products) { this.products = products; }
 * 
 * public Owner getOwner() { return owner; }
 * 
 * public void setOwner(Owner owner) { this.owner = owner; }
 * 
 * public void setOwner(int owner2) { }
 * 
 * public String getOtp() { return otp; }
 * 
 * public void setOtp(String otp) { this.otp = otp; }
 * 
 * // Method to get the total number of products associated with this nursery
 * public int getTotalProducts() { return (this.products != null) ?
 * this.products.size() : 0; }
 * 
 * 
 */
