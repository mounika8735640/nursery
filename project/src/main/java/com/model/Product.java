package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

@Entity

public class Product {
	@Id
    @GeneratedValue
	private Long pId;
	private String productName;
	private String type;
    private String description;
	private Long price;
	private String specifications;
	private String category;
	private String imageName;


	@ManyToOne
	@JoinColumn(name = "nursery_id")
	private Nursery nursery;

	public Product() {
	}

	public Product( String productName, String type, String description, Long price,
			String specifications, String category,String imageName) {
		this.productName = productName;
		this.type = type;
		this.description = description;
		this.price = price;
		this.specifications = specifications;
		this.category = category;
		this.imageName = imageName;

	}

	public Long getPId() {
		return pId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public String getSpecifications() {
		return specifications;
	}

	public void setSpecifications(String specifications) {
		this.specifications = specifications;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public Integer getNurseryId() {
		return (nursery != null) ? nursery.getNurseryId() : null;
	}

	

	
}