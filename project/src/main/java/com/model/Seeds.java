package com.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToOne;

@Entity

public class Seeds{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int seedId;
	private String seedName;
	private String description;
	private String type;
	@Lob
	  private byte[] image;
	private Long price;
	
	 @OneToOne
	    private Product product;
	
	public Seeds(){
		
	}
	
	public Seeds(int seedId, String seedName, String description, String type, byte[] image, Long price) {
	
		this.seedId = seedId;
		this.seedName = seedName;
		this.description = description;
		this.type = type;
		this.image=image;
		this.price = price;
	}
	
	public Seeds( String seedName, String description, String type,byte[] image, Long price) {
		
		this.seedName = seedName;
		this.description = description;
		this.type = type;
		this.image=image;
		this.price = price;
	}

	public int getSeedId() {
		return seedId;
	}

	public String getSeedName() {
		return seedName;
	}

	public void setSeedName(String seedName) {
		this.seedName = seedName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}
	
}
