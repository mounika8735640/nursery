package com.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToOne;

@Entity

public class Fertilizers {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int fertilizersId;
	private String fertilizerName;
	private String types;
	private String specifications;
	private String description;
	@Lob
	private byte[] image;
	private Long price;

	@OneToOne
	private Product product;

	public Fertilizers() {

	}

	public Fertilizers(String fertilizerName, String types, String specifications, String description, byte[] image,
			Long price) {

		this.fertilizerName = fertilizerName;
		this.types = types;
		this.specifications = specifications;
		this.description = description;
		this.image = image;
		this.price = price;
	}

	public Fertilizers(int fertilizersId, String fertilizerName, String types, String specifications,
			String description, byte[] image, Long price) {

		this.fertilizersId = fertilizersId;
		this.fertilizerName = fertilizerName;
		this.types = types;
		this.specifications = specifications;
		this.description = description;
		this.image = image;
		this.price = price;
	}

	public int getFertilizersId() {
		return fertilizersId;
	}


	public String getFertilizerName() {
		return fertilizerName;
	}

	public void setFertilizerName(String fertilizerName) {
		this.fertilizerName = fertilizerName;
	}

	public String getTypes() {
		return types;
	}

	public void setTypes(String types) {
		this.types = types;
	}

	public String getSpecifications() {
		return specifications;
	}

	public void setSpecifications(String specifications) {
		this.specifications = specifications;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

}
