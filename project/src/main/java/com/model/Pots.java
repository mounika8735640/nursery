package com.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToOne;

@Entity

public class Pots {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long potId;
	private String potName;
	private String type;
	private String specifications;
	private String description;
	@Lob
	private byte[] image;
	private Long price;
	
	 @OneToOne
	    private Product product;

	public Pots() {

	}

	public Pots(String potName, String type, String specifications, String description, byte[] image, Long price) {

		this.potName = potName;
		this.type = type;
		this.specifications = specifications;
		this.description = description;
		this.image = image;
		this.price = price;
	}

	public Pots(Long potId, String potName, String type, String specifications, String description, byte[] image, Long price) {

		this.potId = potId;
		this.potName = potName;
		this.type = type;
		this.specifications = specifications;
		this.description = description;
		this.image = image;
		this.price = price;
	}

	public Long getPotId() {
		return potId;
	}

	public String getPotName() {
		return potName;
	}

	public void setPotName(String potName) {
		this.potName = potName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSpecifications() {
		return specifications;
	}

	public void setSpecifications(String specifications) {
		this.specifications = specifications;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

}
