package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Faqs {

	@Id
	@GeneratedValue
	private Long fid;
	private String questions;
	private String answers;

	private Faqs() {

	}

	public Faqs(String questions, String answers) {
		this.questions = questions;
		this.answers = answers;
	}

	public Long getFid() {
		return fid;
	}

	public String getQuestions() {
		return questions;
	}

	public void setQuestions(String questions) {
		this.questions = questions;
	}

	public String getAnswers() {
		return answers;
	}

	public void setAnswers(String answers) {
		this.answers = answers;
	}

}
