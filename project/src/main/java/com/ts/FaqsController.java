package com.ts;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.dao.FaqsDao;
import com.model.Faqs;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping

public class FaqsController {
	@Autowired
	FaqsDao faqsDao;

	@GetMapping("getFaqs")
	public List<Faqs> getFaqs() {
		return faqsDao.getFaqs();
	}

	@GetMapping("getFaqsById/{fId}")
	public Faqs getFaqsById(@PathVariable Long fId) {
		return faqsDao.getFaqsById(fId);
	}

}
