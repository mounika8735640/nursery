package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.NurseryDao;
import com.model.Credentials;
import com.model.Nursery;
import com.model.User;

import com.model.Owner;

@RestController
@CrossOrigin(origins = "http://localhost:4200")

@RequestMapping
public class NurseryController {

	@Autowired
	private NurseryDao nurseryDao;

	@GetMapping("getAllNurseries")
	public List<Nursery> getAllNurseries() {
		return nurseryDao.getAllNurseries();
	}

	@GetMapping("getNurseryById/{nurseryId}")
	public Nursery getNurseryById(@PathVariable int nurseryId) {
		return nurseryDao.getNurseryById(nurseryId);
	}

	@GetMapping("getNurseryByName/{nurseryName}")
	public Nursery getNurseryByName(@PathVariable String nurseryName) {
		return nurseryDao.getNurseryByName(nurseryName);
	}

	@GetMapping("nurseryLogin/{emailId}/{password}")
	public Nursery nurseryLogin(@PathVariable String emailId, @PathVariable String password) {
		return nurseryDao.nurseryLogin(emailId, password);

	}

	@GetMapping("getNurseryEmailId/{emailId}")
	public Nursery getNurseryEmailId(@PathVariable String emailId) {
		return nurseryDao.getNurseryEmailId(emailId);
	}

	@PostMapping("addNursery")
	public Nursery addNursery(@RequestBody Nursery nursery) {

		System.out.println(nursery);

		// return nursery;

		return nurseryDao.addNursery(nursery);
	}

	@PutMapping("updateNursery")
	public Nursery updateNursery(@RequestBody Nursery nursery) {
		return nurseryDao.updateNursery(nursery);
	}

	@PutMapping("updateNurseryPassword")
	public Nursery updateNurseryPassword(@RequestBody Credentials cred) {
		System.out.println(cred.getEmailId());
		System.out.println(cred.getPassword());

		return nurseryDao.updateNurseryPassword(cred.getEmailId(), cred.getPassword());
	}

	@DeleteMapping("deleteNurseryById/{nurseryId}")
	public void deleteNursery(@PathVariable int nurseryId) {
		nurseryDao.deleteNursery(nurseryId);
	}
}
