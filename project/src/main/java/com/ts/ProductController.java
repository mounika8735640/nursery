package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.ProductDao;
import com.model.Product;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class ProductController {

	@Autowired
	ProductDao productDao;

	@GetMapping("getProducts")
	public List<Product> getProducts() {
		return productDao.getProducts();
	}

	@GetMapping("getProductById/{pId}")
	public Product getProductById(@PathVariable Long pId) {
		return productDao.getProductById(pId);
	}

	@GetMapping("getProductByNurseryId/{nursery_id}")
	public List<Product>  getProductByNurseryId(@PathVariable int nursery_id) {
		return productDao.getProductByNurseryId(nursery_id);
	}

	@GetMapping("getProductByName/{productName}")
	public ResponseEntity<Product> getProductByName(@PathVariable String productName) {
		Product product = productDao.getProductByName(productName);

		if (product != null) {
			return ResponseEntity.ok(product);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@GetMapping("getProductByPlantCategory/{plant}")
	public List<Product> getProductByPlantCategory(@PathVariable String plant) {
		return productDao.getProductByPlantCategory(plant);
	}

	@GetMapping("getProductByPotCategory/{pot}")
	public List<Product> getProductByPotCategory(@PathVariable String pot) {
		return productDao.getProductByPotCategory(pot);
	}

	@GetMapping("getProductBySoilCategory/{soil}")
	public List<Product> getProductBySoilCategory(@PathVariable String soil) {
		return productDao.getProductBySoilCategory(soil);
	}

	@GetMapping("getProductByPesticideCategory/{pesticide}")
	public List<Product> getProductByPesticideCategory(@PathVariable String pesticide) {
		return productDao.getProductByPesticideCategory(pesticide);
	}

	@GetMapping("getProductBySeedCategory/{seed}")
	public List<Product> getProductBySeedCategory(@PathVariable String seed) {
		return productDao.getProductByPesticideCategory(seed);
	}

	@GetMapping("getProductByFertilizerCategory/{fertilizer}")
	public List<Product> getProductByFertilizerCategory(@PathVariable String fertilizer) {
		return productDao.getProductByFertilizerCategory(fertilizer);
	}

	@GetMapping("getProductByToolCategory/{tool}")
	public List<Product> getProductByToolCategory(@PathVariable String tool) {
		return productDao.getProductByToolCategory(tool);
	}

	@GetMapping("getProductBySpecifications/{specifications}")
	public Product getProductBySpecifications(@PathVariable String specifications) {
		return productDao.getProductBySpecifications(specifications);
	}

	@GetMapping("getProductByPrice/{price}")
	public Product getProductByPrice(@PathVariable Long price) {
		return productDao.getProductByPrice(price);
	}

	@PostMapping("addProduct")
	public Product addProduct(@RequestBody Product product) {
		return productDao.addProduct(product);
	}

	@PutMapping("updateProduct")
	public Product updateProduct(@RequestBody Product product) {
		return productDao.updateProduct(product);
	}

	@DeleteMapping("deleteProductById/{pId}")
	public String deleteProductById(@PathVariable Long pId) {
		productDao.deleteProductById(pId);
		return "Product with product Id:" + pId + " Deleted Successfully!!!";
	}
}
