package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.OwnerDao;
import com.model.Credentials;
import com.model.Owner;

@RestController
@CrossOrigin(origins = "http://localhost:4200")

public class OwnerController {
	@Autowired
	OwnerDao ownerDao;

	@GetMapping("getOwners")
	public List<Owner> getOwners() {
		return ownerDao.getOwners();
	}

	@GetMapping("getOwnerById/{id}")
	public Owner getOwnerById(@PathVariable Long id) {
		return ownerDao.getOwnerById(id);
	}

	@GetMapping("getOwnerByName/{name}")
	public Owner getOwnerByName(@PathVariable String name) {
		return ownerDao.getOwnerByName(name);
	}

	@GetMapping("getOwnerByLocation/{location}")
	public Owner getOwnerByLocation(String location) {
		return ownerDao.getOwnerByLocation(location);
	}

	@GetMapping("ownerLogin/{email_Id}/{password}")
	public Owner ownerLogin(@PathVariable String email_Id, @PathVariable String password) {
		return ownerDao.ownerLogin(email_Id, password);

	}

	@GetMapping("getOwnerEmailId/{emailId}")
	public Owner getOwnerEmailId(@PathVariable String emailId) {
		return ownerDao.getOwnerEmailId(emailId);
	}

	@PostMapping("addOwner")
	public Owner addOwner(@RequestBody Owner owner) {
		return ownerDao.addOwner(owner);
	}

	@PutMapping("updateOwner")
	public Owner updateOwner(@RequestBody Owner owner) {
		return ownerDao.updateOwner(owner);
	}

	@PutMapping("updateOwnerPassword")
	public Owner updateOwnerPassword(@RequestBody Credentials cred) {
		System.out.println(cred.getEmailId());
		System.out.println(cred.getPassword());

		return ownerDao.updateOwnerPassword(cred.getEmailId(), cred.getPassword());
	}

	@DeleteMapping("deleteOwnerById/{ownerId}")
	public String deleteOwnerById(@PathVariable Long ownerId) {
		ownerDao.deleteOwnerById(ownerId);
		return "User with UserId: " + ownerId + " deleted successfully";

	}

}
