package com.ts;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.dao.CartDao;
import com.model.Cart;

@RestController
@CrossOrigin(origins = "http://localhost:4200")

@RequestMapping
public class CartController {

	@Autowired
	private CartDao cartDao;

	@GetMapping("getAllCarts")
	public List<Cart> getAllCarts() {
		return cartDao.getAllCarts();
	}

	@GetMapping("getCartById/{cart_id}")
	public Cart getCartById(@PathVariable Long cart_id) {
		return cartDao.getCartById(cart_id);
	}

	@PostMapping("addCart")
	public Cart addCart(@RequestBody Cart cart) {
		return cartDao.addCart(cart);

	}

	@PutMapping("updateCart")
	public Cart updateCart(@RequestBody Cart cart) {
		return cartDao.updateCart(cart);
	}

	@DeleteMapping("deleteCartById/{cart_id}")
	public String deleteCartById(@PathVariable Long cart_id) {
		cartDao.deleteCartById(cart_id);
		return "User with UserId: " + cart_id + " deleted successfully";
	}
}
