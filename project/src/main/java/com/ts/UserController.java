package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.dao.UserDao;
import com.model.Credentials;
import com.model.User;

@RestController
@CrossOrigin(origins = "http://localhost:4200")

public class UserController {

	@Autowired
	UserDao userDao;


  

    @GetMapping("getUsers")
    public List<User> getUsers() {
        return userDao.getUsers();
    }

    @GetMapping("getUserById/{userId}")
    public User getUserById(@PathVariable Long userId) {
        return userDao.getUserById(userId);
    }

	@GetMapping("getUserEmailId/{emailId}")
	public User getUserEmailId(@PathVariable String emailId) {
		return userDao.getUserEmailId(emailId);
	}

	@GetMapping("loginUser/{emailId}/{password}")
	public User userLogin(@PathVariable String emailId, @PathVariable String password) {
		return userDao.loginUser(emailId, password);
	}

	@PostMapping("addUser")
	public String addUser(@RequestBody User user) {
		return userDao.addUser(user);
	}
    @GetMapping("getUserByName/{userName}")
    public User getUserByName(@PathVariable String userName) {
        return userDao.getUserByName(userName);
    }

    @PostMapping("loginUser")
    public ResponseEntity<String> loginUser(@RequestParam String emailId, @RequestParam String password) {
        try {
            User loggedInUser = userDao.loginUser(emailId, password);
            return ResponseEntity.ok("Login successful. User ID: " + loggedInUser);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Invalid email or password");
        }
    }

	@PutMapping("updateUserPassword")
	public User updateUserPassword(@RequestBody Credentials cred) {
		System.out.println(cred.getEmailId());
		System.out.println(cred.getPassword());

		return userDao.updateUserPassword(cred.getEmailId(), cred.getPassword());
	}

	@DeleteMapping("deleteUserById/{userId}")
	public String deleteUserById(@PathVariable Long userId) {
		userDao.deleteUserById(userId);
		return "User with UserId: " + userId + " deleted successfully";

	}
    @PutMapping("updateUser")
    public ResponseEntity<User> updateUser(@RequestBody User user) {
        try {
            User updatedUser = userDao.updateUser(user);
            return ResponseEntity.ok(updatedUser);
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

  
}
