package com.ts;

import com.dao.PlantsDao;
import com.model.Plants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("plants")
public class PlantsController {

    @Autowired
    private PlantsDao plantsDao;

    @GetMapping("getAllPlants")
    public List<Plants> getAllPlants() {
        return plantsDao.getAllPlants();
    }

    @GetMapping("getPlantById/{plantId}")
    public ResponseEntity<Plants> getPlantById(@PathVariable int plantId) {
        Plants plant = plantsDao.getPlantById(plantId);
        if (plant != null) {
            return ResponseEntity.ok(plant);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("getPlantByName/{plantName}")
    public List<Plants> getPlantByName(@PathVariable String plantName) {
        return plantsDao.getPlantByName(plantName);
    }

    @GetMapping("getPlantByPrice/{price}")
    public List<Plants> getPlantByPrice(@PathVariable Long price) {
        return plantsDao.getPlantByPrice(price);
    }

    @GetMapping("getPlantByType/{type}")
    public List<Plants> getPlantByType(@PathVariable String type) {
        return plantsDao.getPlantByType(type);
    }

    @PostMapping("addPlant")
    public Plants addPlant(@RequestBody Plants plant) {
        return plantsDao.addPlant(plant);
    }

    @PutMapping("updatePlant")
    public Plants updatePlant(@RequestBody Plants plant) {
        return plantsDao.updatePlant(plant);
    }

    @DeleteMapping("deletePlant/{plantId}")
    public ResponseEntity<String> deletePlant(@PathVariable int plantId) {
        try {
            plantsDao.deletePlant(plantId);
            return ResponseEntity.ok("Plant with ID: " + plantId + " deleted successfully");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Error deleting plant: " + e.getMessage());
        }
    }
}
