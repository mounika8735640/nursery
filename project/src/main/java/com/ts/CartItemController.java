package com.ts;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.CartItemDao;
import com.model.CartItem;

@RestController
@CrossOrigin(origins = "http://localhost:4200")

@RequestMapping
public class CartItemController {

	@Autowired
	private CartItemDao cartItemDao;

	@GetMapping("getCartItemById{cartItemId}")
	public CartItem getCartItem(@PathVariable Long cartItemId) {
		return cartItemDao.getCartItemById(cartItemId);
	}

	@GetMapping("getAllCartItems")
	public List<CartItem> getAllCartItems() {
		return cartItemDao.getAllCartItems();
	}

	@PostMapping("addCartItem")
	public CartItem addCartItem(@RequestBody CartItem cartItem) {
		return cartItemDao.addCartItem(cartItem);
	}

	@PutMapping("updateCartItem")
	public CartItem updateCartItem(@RequestBody CartItem cartItem) {
		return cartItemDao.updateCartItem(cartItem);
	}

	@DeleteMapping("deleteCartItemById/{cartItemId}")
	public String deleteCartItemById(@PathVariable Long cartItemId) {
		cartItemDao.deleteCartItemById(cartItemId);
		return "Item with CartItemId: " + cartItemId + " deleted successfully";
	}
}
