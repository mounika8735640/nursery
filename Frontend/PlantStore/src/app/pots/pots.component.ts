import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-plants',
  templateUrl: './pots.component.html',
  styleUrls: ['./pots.component.css']
})
export class PotsComponent implements OnInit {
  pots: any[] = [];

  constructor(private service: UserService) { }

  ngOnInit(): void {
    const pot: string = 'pot'; 

    this.service.getProductByPotCategory(pot).subscribe(
      (pots: any[]) => {
        this.pots = pots;
      },
      error => {
        console.log('Error fetching pots:', error);
      }
    );
  }
}