import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-nurseryinfo',
  templateUrl: './nurseryinfo.component.html',
  styleUrls: ['./nurseryinfo.component.css']
})
export class NurseryinfoComponent implements OnInit {
  error: string | null = null;
  selectedNursery: any;
  loading: boolean = true;
  nurseryProducts: any[] = [];

  constructor(
    private route: ActivatedRoute,
    private service: UserService
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const nurseryName = params['nurseryName'];

      if (nurseryName !== undefined && nurseryName !== '') {
        this.service.getNurseryByName(nurseryName).subscribe(
          (nursery: any) => {
            if (nursery) {
              this.selectedNursery = nursery;
              this.loading = false;

              this.service.getProductByNurseryId(this.selectedNursery.nurseryId).subscribe(
                (products: any[]) => {
                  this.nurseryProducts = products;
                },
                (error: any) => {
                  console.log('Error fetching products for the nursery: ', error);
                  this.error = 'Error fetching products information';
                }
              );
            } else {
              this.error = 'Nursery not found';
              this.loading = false;
            }
          },
          (error: any) => {
            console.log('Error fetching the nurseries data: ', error);
            this.error = 'Error fetching nurseries information';
            this.loading = false;
          }
        );
      } else {
        console.log('Invalid nurseries name');
        this.error = 'Invalid nurseries name';
        this.loading = false;
      }
    });
  }
}
