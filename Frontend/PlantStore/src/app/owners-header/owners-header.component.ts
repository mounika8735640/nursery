// owners-header.component.ts

import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-owners-header',
  templateUrl: './owners-header.component.html',
  styleUrls: ['./owners-header.component.css']
})
export class OwnersHeaderComponent implements OnInit {
  slides = [
    { image: './assets/C1.webp' },
    { image: './assets/C2.webp' },
    { image: './assets/C4.webp' },
    { image: './assets/C5.webp' },
  ];

  loginStatus: any;

  constructor(private service: UserService) {
  }

  ngOnInit() {
    this.service.getLoginStatus().subscribe((data: any) => {
      this.loginStatus = data;
    });
  }
}
