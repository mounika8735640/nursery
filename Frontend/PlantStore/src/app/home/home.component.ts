import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  faqs: any[] = [];
  activeFaq: any = null;

  faqStyles: any = {
    questionClass: 'faq-question',
    answerClass: 'faq-answer'
  };
  loginStatus: any;
  activeSlideIndex = 0;
  slides = [
    { image: './assets/C1.webp' },
    { image: './assets/C2.webp' },
    { image: './assets/C4.webp' },
    { image: './assets/C5.webp' },
  ];
  searchQuery: any;
  products: any[] = [];

  constructor(private service: UserService, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.service.getLoginStatus().subscribe((data: any) => {
      this.loginStatus = data;
    });

    this.service.getProducts().subscribe((data: any[]) => {
      this.products = data;
      this.getFaqs();

    });

  }
  

  prevSlide() {
    this.activeSlideIndex = (this.activeSlideIndex === 0) ? this.slides.length - 1 : this.activeSlideIndex - 1;
  }

  nextSlide() {
    this.activeSlideIndex = (this.activeSlideIndex === this.slides.length - 1) ? 0 : this.activeSlideIndex + 1;
  }

  search() {
    if (this.searchQuery.trim() !== '') {
      console.log('Performing search for:', this.searchQuery);
      this.service.getProductByName(this.searchQuery).subscribe((results: any[]) => {
        if (results && results.length === 0) {
          this.toastr.error('No matching results found!', 'Error');
        } else {
          console.log('Search results:', results);
        }
      });
    }
    if (this.searchQuery.trim() !== '') {
      console.log('Performing search for:', this.searchQuery);
      this.service.getNurseryByName(this.searchQuery).subscribe((results: any[]) => {
        if (results && results.length === 0) {
          this.toastr.error('No matching results found!', 'Error');
        } else {
          console.log('Search results:', results);
        }
      });
    }
  }

 
  getFaqs(): void {
    this.service.getFaqs().subscribe(
      (data) => {
        this.faqs = data;
      },
      (error) => {
        console.error('Error fetching FAQs:', error);
      }
    );
  }
  toggleAnswer(faq: any): void {
    this.activeFaq = (this.activeFaq === faq) ? null : faq;
  }
}
