import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

interface Nursery {
  nurseryId?: number;
  imageName: string;
  nurseryName: string;
  location: string;
  phoneNumber: number;
  emailId: string;
  ownerId?: number;
  
}
@Component({
  selector: 'app-nursery',
  templateUrl: './nursery.component.html',
  styleUrl: './nursery.component.css'
})
export class NurseryComponent implements OnInit{
  nurseries: Nursery[] = [];
  loading: boolean = true;
  error: string | null = null;

  constructor(private service: UserService) { }

  ngOnInit(): void {
    this.service.getAllNurseries().subscribe(
      (nurseries: Nursery[]) => {
        console.log(nurseries); 
        this.nurseries = nurseries;
        this.loading = false; 
      },
      error => {
        console.log('Error fetching the Nurseries data: ', error);
        this.error = 'Error fetching nurseries data'; 
        this.loading = false; 
      }
    );
  }
}
