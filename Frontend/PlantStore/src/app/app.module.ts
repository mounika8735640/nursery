import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { ToastrModule } from 'ngx-toastr';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';
import { AdminHeaderComponent } from './admin-header/admin-header.component';
import { HomeComponent } from './home/home.component';
import { Login1Component } from './login1/login1.component';
import { LogoutComponent } from './logout/logout.component';
import { OwnersHeaderComponent } from './owners-header/owners-header.component';
import { PlantsComponent } from './plants/plants.component';
import { PesticidesComponent } from './pesticides/pesticides.component';
import { ProductsComponent } from './products/products.component';
import { PotsComponent } from './pots/pots.component';
import { SeedsComponent } from './seeds/seeds.component';
import { ShowownersComponent } from './showowners/showowners.component';
import { ShownurseriesComponent } from './shownurseries/shownurseries.component';
import { ShowusersComponent } from './showusers/showusers.component';
import { SoilComponent } from './soil/soil.component';
import { ToolsComponent } from './tools/tools.component';
import { fertilizersComponent } from './fertilizers/fertilizers.component';
import { UserheaderComponent } from './userheader/userheader.component';
import { Register1Component } from './register1/register1.component';
import { ForgotPasswordComponent } from './forgotpassword/forgotpassword.component';
import { PasswordComponent } from './password/password.component';
import { OwnerloginComponent } from './ownerlogin/ownerlogin.component';
import { OwnerregisterComponent } from './ownerregister/ownerregister.component';
import { NurseryloginComponent } from './nurserylogin/nurserylogin.component';
import { CartComponent } from './cart/cart.component';
import { ProductinfoComponent } from './productinfo/productinfo.component';
import { NurseryComponent } from './nursery/nursery.component';
import { NurseryinfoComponent } from './nurseryinfo/nurseryinfo.component';
import { ContactComponent } from './contact/contact.component';
import { AddproductComponent } from './addproduct/addproduct.component';
import { RECAPTCHA_SETTINGS, RecaptchaFormsModule, RecaptchaModule, RecaptchaSettings } from 'ng-recaptcha';
import { FirstComponent } from './first/first.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NurseryregisterComponent } from './nurseryregister/nurseryregister.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminHeaderComponent,
    OwnersHeaderComponent,
    LogoutComponent,
    ShowusersComponent,
    ShownurseriesComponent,
    ShowownersComponent,
    PlantsComponent,
    SoilComponent,
    SeedsComponent,
    PotsComponent,
    ToolsComponent,
    PesticidesComponent,
    fertilizersComponent,
    UserheaderComponent,
    ProductsComponent,
    AboutComponent,
    HomeComponent,
    Login1Component,
    Register1Component,
    ForgotPasswordComponent,
    PasswordComponent,
    OwnerloginComponent,
    OwnerregisterComponent,
    NurseryloginComponent,
    CartComponent,
    ProductinfoComponent,
    NurseryComponent,
    NurseryinfoComponent,
    ContactComponent,
    AddproductComponent,
    FirstComponent,
    NurseryregisterComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    CarouselModule.forRoot(),
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    RecaptchaModule,
    RecaptchaFormsModule,
    HttpClientModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
    }),
    RouterModule,
  ],
  providers: [
    {
      provide: RECAPTCHA_SETTINGS,
      useValue: {
        siteKey: '6LfOvmcpAAAAADldH5zEm5pYQe0s7aRLQupOewmM', // Replace with your Site Key
      } as RecaptchaSettings,
    },
  ],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA],
})
export class AppModule {}
