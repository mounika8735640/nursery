// showowners.component.ts

import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

declare var jQuery: any;

@Component({
  selector: 'app-showowners',
  templateUrl: './showowners.component.html',
  styleUrls: ['./showowners.component.css']
})
export class ShowownersComponent implements OnInit {
  owners: any;
  emailId: any;

  constructor(private service: UserService) {
    this.emailId = localStorage.getItem('emailId');

  }

  ngOnInit() {
    this.service.getOwners().subscribe((data: any) => {
      this.owners = data;
    });
  }




  deleteOwner(owner: any) {
    this.service.deleteOwner(owner.ownerId).subscribe((data: any) => {
      console.log(data);
    });

    const index = this.owners.findIndex((element: any) => element.ownerId === owner.ownerId);
    if (index !== -1) {
      this.owners.splice(index, 1);
    }
    alert('Owner Deleted Successfully!!!');
  }
}
