import { Component ,OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrl: './logout.component.css'
})
export class LogoutComponent {
  constructor(private router: Router, private service: UserService) {

    localStorage.removeItem('emailId');
    localStorage.clear();

    //Setting the isUserLoggedIn variable value to false under EmpService
    this.service.setIsUserLoggedOut();

    alert('Successfully Logged Out');
    this.router.navigate(['/login1']);
  }

  ngOnInit() {
  }

}
