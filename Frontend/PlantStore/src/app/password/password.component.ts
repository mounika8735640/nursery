import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../user.service';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.css']
})
export class PasswordComponent {
  emailId: string = '';
  password:string ='';
  user: any;

  constructor(private service: UserService, private toastr: ToastrService, private router: Router) {
    this.user = {
      emailId: '',
      password: ''
    };
  }

  ngOnInit() {
    this.service.currentEmailId.subscribe(emailId => {
      this.emailId = emailId;
    });
  }

  passwordChanged(regForm: any) {
    this.user.emailId = regForm.emailId;
    this.user.password = regForm.password;

    console.log(this.user.emailId);
    console.log(this.user.password);

    this.service.updateUserPassword(this.user).subscribe((data: any) => { console.log(data); });
    this.toastr.success('password changed');
    this.router.navigate(['login1']);
  }
}
