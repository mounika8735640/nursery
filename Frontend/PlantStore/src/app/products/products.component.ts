// products.component.ts
import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

interface Product {
  pid: number;
  imageName: string;
  productName: string;
  category: string;
  price: number;
  description: string;
  specifications: string;
  type: string;
  nurseryId?: number;
}

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products: Product[] = [];
  loading: boolean = true;
  error: string | null = null;

  constructor(private service: UserService) { }

  ngOnInit(): void {
    this.service.getProducts().subscribe(
      (products: Product[]) => {
        console.log(products); 
        this.products = products;
        this.loading = false; 
      },
      error => {
        console.log('Error fetching the Products data: ', error);
        this.error = 'Error fetching products data'; 
        this.loading = false; 
      }
    );
  }
}
