import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register1',
  templateUrl: './register1.component.html',
  styleUrls: ['./register1.component.css']
})
export class Register1Component implements OnInit {
  user: any = {
    emailId: '',
    location: '',
    password: '',
    confirmPassword: '',
    phoneNumber: '',
    username: '',
    remember: false
  };
  captchaResponse: string = '';

  constructor(
    private userService: UserService,
    private router: Router,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    console.log('RegisterComponent initialized');
  }

  submitRegistrationForm() {
    console.log('Registration Form submitted');
    console.log('User Name:', this.user.username);
    console.log('Email Id:', this.user.emailId);
    console.log('Password:', this.user.password);
    console.log('Confirm Password:', this.user.confirmPassword);
    console.log('Location:', this.user.location);
    console.log('Phone Number:', this.user.phoneNumber);
    
    if (this.user.password !== this.user.confirmPassword) {
      console.log('Password and confirmpassword must be same.');
      return;
    }
    this.userService.registerUser(this.user).subscribe(
      (data: any) => {
        console.log('Registration successful: ', data);
        this.toastr.success('Registration successful', 'Success');
        console.log('Navigating to login1...');
        this.router.navigate(['login1']);
        console.log('Navigation completed.');
      },
      (error: any) => {
        this.toastr.error('Registration Failed', 'Error');
        console.error('Registration failed: ', error);
      }
    );
  }
  // handleCaptchaResponse(captchaResponse: string) {
  //   this.captchaResponse = captchaResponse;
  // }
}
