// ownerregister.component.ts
import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-ownerregister',
  templateUrl: './ownerregister.component.html',
  styleUrls: ['./ownerregister.component.css'],
})
export class OwnerregisterComponent implements OnInit {
  captchaResponse: string = '';

  owner: any = {
    emailId: '',
    location: '',
    password: '',
    confirmPassword: '',
    phoneNumber: '',
    name: '',
    remember: false
  };

  constructor(
    private userService: UserService,
    private router: Router,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    console.log('OwnerRegisterComponent initialized');
  }

  submitRegistrationForm() {
    console.log('Registration Form submitted');
    console.log('Owner Name:', this.owner.name);
    console.log('Email Id:', this.owner.emailId);
    console.log('Password:', this.owner.password);
    console.log('Confirm Password:', this.owner.confirmPassword);
    console.log('Location:', this.owner.location);
    console.log('Phone Number:', this.owner.phoneNumber);
    if (this.owner.password !== this.owner.confirmPassword) {
      console.log('Password and confirmpassword must be the same.');
      return;
    }
    this.userService.registerOwner(this.owner).subscribe(
      (data: any) => {
        console.log('Registration successful: ', data);
        this.toastr.success('Registration successful', 'Success');
        console.log('Navigating to ownerlogin...');
        this.router.navigate(['ownerlogin']);
        console.log('Navigation completed.');
      },
      (error: any) => {
        this.toastr.error('Registration Failed', 'Error');
        console.error('Registration failed: ', error);
      }
    );
  }

}
