import { Component } from '@angular/core';

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrl: './admin-header.component.css'
})
export class AdminHeaderComponent {
  activeSlideIndex = 0;
  slides = [
    { image: './assets/images/A1.jpg' },
    { image: './assets/images/A2.jpg' },
    { image: './assets/C5.webp' },


  ];
  prevSlide() {
    this.activeSlideIndex = (this.activeSlideIndex === 0) ? this.slides.length - 1 : this.activeSlideIndex - 1;
  }
  nextSlide() {
    this.activeSlideIndex = (this.activeSlideIndex === this.slides.length - 1) ? 0 : this.activeSlideIndex + 1;
  }


}
