import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../user.service';

declare var google: any;

@Component({
  selector: 'app-login1',
  templateUrl: './login1.component.html',
  styleUrls: ['./login1.component.css']
})
export class Login1Component  {
  emailId: string = '';
  password: string = '';
  captchaResponse: string = '';
  showPassword:string ='';

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private userService: UserService
  ) { }
  ngOnInit(): void {
    google.accounts.id.initialize({
      client_id: '364713573542-2eua08udek4c712gc9c5jfo2ohbsqema.apps.googleusercontent.com',
      callback: (resp: any) => this.handleLogin(resp)

    });

    google.accounts.id.renderButton(document.getElementById("google-btn"), {
      theme: 'filled_blue',
      size: 'large',
      shape: 'rectangle',
      width: 350,
    })

  }

  async loginSubmit(loginForm: any) {
    if (loginForm.invalid) {
      this.toastr.error('Please enter both email and password.', 'Error');
      return;
    }

    this.emailId = loginForm.value.emailId;
    this.password = loginForm.value.password;

    if (this.emailId === 'mounikamouni8311@gmail.com' && this.password === 'Mounika@123') {
      localStorage.setItem('isAdmin', 'true');
      this.router.navigate(['admin-navbar']);
      return;
    }

    const isValidCredentials = await this.userService.userLogin(this.emailId, this.password);

    if (isValidCredentials) {
      localStorage.setItem('emailId', this.emailId);
      this.userService.setIsUserLoggedIn();
      this.router.navigate(['userheader']);
      this.toastr.success('Login Successful.', 'Success');

    } else {
      this.toastr.error('Invalid email or password.', 'Error');
    }
  }

  handleCaptchaResponse(captchaResponse: string) {
    this.captchaResponse = captchaResponse;
  }
  private decodeToken(token: string) {
    return JSON.parse(atob(token.split(".")[1]));
  }

  handleLogin(response: any) {
    console.log("working");
    if (response) {
      const payLoad = this.decodeToken(response.credential)
      sessionStorage.setItem("loggedInUser", JSON.stringify(payLoad));
      localStorage.setItem("email", payLoad.email);
      this.userService.setIsUserLoggedIn();
      this.router.navigate(['userheader']);
    }
  }
}
