import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrl: './addproduct.component.css'
})
export class AddproductComponent implements OnInit{
  product: any = {
    productName: '',
    price: 0,
    specifications: '',
    category: '',
    type: '',
    imageName: '',
    description: '',
    nurseryId:''
  };

  constructor(private service: UserService, private toastr: ToastrService,private router :Router) {}
  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }

  submitProductForm() {
    this.service.addProduct(this.product).subscribe(
      (data: any) => {
        console.log('Product added successfully: ', data);
        this.toastr.success('Product added successfully', 'Success');
        this.router.navigate(['owners-navbar']);
      },
      (error: any) => {
        this.toastr.error('Failed to add product', 'Error');
        console.error('Failed to add product: ', error);
      }
    );
  }

}
