import { Component } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-soil',
  templateUrl: './soil.component.html',
  styleUrl: './soil.component.css'
})
export class SoilComponent {
  soils: any[] = [];

  constructor(private service: UserService) { }

  ngOnInit(): void {
    // Assuming 'plant' is a string representing a category
    const soil: string = 'soil'; // Define your category here

    this.service.getProductBySoilCategory(soil).subscribe(
      (soils: any[]) => {
        this.soils = soils;
      },
      error => {
        console.log('Error fetching Soil:', error);
      }
    );
  }
}



