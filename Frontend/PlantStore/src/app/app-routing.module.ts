import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Register1Component } from './register1/register1.component';
import { Login1Component } from './login1/login1.component';
import { OwnersHeaderComponent } from './owners-header/owners-header.component';
import { authGuard } from './auth.guard';
import { LogoutComponent } from './logout/logout.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ForgotPasswordComponent } from './forgotpassword/forgotpassword.component';
import { ShownurseriesComponent } from './shownurseries/shownurseries.component';
import { ShowownersComponent } from './showowners/showowners.component';
import { AdminHeaderComponent } from './admin-header/admin-header.component';
import { ShowusersComponent } from './showusers/showusers.component';
import { UserheaderComponent } from './userheader/userheader.component';
import { PasswordComponent } from './password/password.component';
import { OwnerloginComponent } from './ownerlogin/ownerlogin.component';
import { OwnerregisterComponent } from './ownerregister/ownerregister.component';
import { PlantsComponent } from './plants/plants.component';
import { CartComponent } from './cart/cart.component';
import { ProductsComponent } from './products/products.component';
import { ProductinfoComponent } from './productinfo/productinfo.component';
import { PotsComponent } from './pots/pots.component';
import { SoilComponent } from './soil/soil.component';
import { fertilizersComponent } from './fertilizers/fertilizers.component';
import { PesticidesComponent } from './pesticides/pesticides.component';
import { ToolsComponent } from './tools/tools.component';
import { SeedsComponent } from './seeds/seeds.component';
import { NurseryComponent } from './nursery/nursery.component';
import { NurseryloginComponent } from './nurserylogin/nurserylogin.component';
import { ContactComponent } from './contact/contact.component';
import { NurseryinfoComponent } from './nurseryinfo/nurseryinfo.component';
import { AddproductComponent } from './addproduct/addproduct.component';
import { FirstComponent } from './first/first.component';
import { NurseryregisterComponent } from './nurseryregister/nurseryregister.component';

const routes: Routes = [
  { path: '', component: FirstComponent },
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'nurseryregister', component: NurseryregisterComponent },
  { path: 'home', component: HomeComponent },
  { path: 'owners-navbar', component: OwnersHeaderComponent },
  { path: 'admin-navbar', component: AdminHeaderComponent },
  { path: 'showusers', component: ShowusersComponent },
  { path: 'ownerlogin', component: OwnerloginComponent },
  { path: 'ownerregister', component: OwnerregisterComponent },
  { path: 'userheader', component: UserheaderComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'showowners', component: ShowownersComponent }, // Example route for showowners component
  { path: 'shownurseries', component: ShownurseriesComponent },
  { path: 'login1', component: Login1Component },
  { path: 'register1', component: Register1Component },
  { path: 'password', component: PasswordComponent },
  { path: 'products', component: ProductsComponent },
  { path: 'productinfo/:productName', component: ProductinfoComponent },
  { path: 'productinfo', component: ProductinfoComponent },
  { path: 'plant', component: PlantsComponent },
  { path: 'pot', component: PotsComponent },
  { path: 'soil', component: SoilComponent },
  { path: 'fertilizer', component: fertilizersComponent },
  { path: 'pesticide', component: PesticidesComponent },
  { path: 'tool', component: ToolsComponent },
  { path: 'seed', component: SeedsComponent },
  { path: 'cart', component: CartComponent },
  { path: 'nurserylogin', component: NurseryloginComponent },
  { path: 'forgotpassword', component: ForgotPasswordComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'nursery', component: NurseryComponent },
  { path: 'nurseryinfo/:nurseryName', component: NurseryinfoComponent },
  { path: 'addproduct', component: AddproductComponent },

];



 

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }