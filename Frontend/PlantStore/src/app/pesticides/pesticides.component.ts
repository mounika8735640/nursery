import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-pesticides',
  templateUrl: './pesticides.component.html',
  styleUrls: ['./pesticides.component.css']
})
export class PesticidesComponent implements OnInit {
  pesticides: any[] = [];

  constructor(private service: UserService) { }

  ngOnInit(): void {
    const pesticideCategory: string = 'pesticide'; 

    this.service.getProductByPesticideCategory(pesticideCategory).subscribe(
      (pesticides: any[]) => {
        this.pesticides = pesticides;
      },
      error => {
        console.log('Error fetching pesticides:', error);
      }
    );
  }
}