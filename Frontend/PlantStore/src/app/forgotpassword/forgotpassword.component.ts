import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder } from '@angular/forms';



@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  otp1: any;
  formModel: any = {};
  error: string = '';


  constructor(private service: UserService, private router: Router,private toastr:ToastrService) { }
  ngOnInit() {
  }

  getOtp() {
    this.service.getUserOtp(this.formModel.emailId).subscribe(
      (data: any) => {
        this.otp1 = data.otp;
        console.log(this.otp1);
      },
      (error: any) => {
        console.error('Error fetching workers:', error);
      }
    );

    this.service.changeEmailId(this.formModel.emailId);
  }

  loginSubmit(loginForm: any) {
    if (loginForm.emailId == this.otp1) {
      console.log("Submitted");
      this.router.navigate(['password']);
    }
    else {
      this.toastr.error("Invalid Credentials");
    }
  }
  
}



