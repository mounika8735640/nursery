import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
declare var jQuery: any;

@Component({
  selector: 'app-shownurseries',
  templateUrl: './shownurseries.component.html',
  styleUrls: ['./shownurseries.component.css']
})
export class ShownurseriesComponent implements OnInit {
  nurseries: any;
  emailId: any;
  ownerList: any[] = [];
  userList: any[] = [];

  constructor(private service: UserService) {
    this.emailId = localStorage.getItem('emailId');
   
  }

  ngOnInit() {
    this.service.getAllNurseries().subscribe((data: any) => {
      this.nurseries = data;
    });
    this.service.getOwners().subscribe((owners: any[]) => {
      this.ownerList = owners;
    });
    this.service.getUsers().subscribe((users: any[]) => {
      this.userList = users;
    });
  }

  




  
 
}
