import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-fertilizers',
  templateUrl: './fertilizers.component.html',
  styleUrls: ['./fertilizers.component.css']
})
export class fertilizersComponent implements OnInit {
  fertilizers: any[] = [];

  constructor(private service: UserService) { }

  ngOnInit(): void {
    // Assuming 'plant' is a string representing a category
    const fertilizer: string = 'fertilizer'; // Define your category here

    this.service.getProductByFertilizerCategory(fertilizer).subscribe(
      (fertilizers: any[]) => {
        this.fertilizers = fertilizers;
      },
      error => {
        console.log('Error fetching fertilizer:', error);
      }
    );
  }
}