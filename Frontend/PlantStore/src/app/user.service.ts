import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  [x: string]: any;

  private cartItems: any[] = [];

  productAddedToCart: Subject<void> = new Subject<void>();

  getFilteredProducts() {
    throw new Error('Method not implemented.');
  }
  checkCredentials(emailId: string, password: string) {
    throw new Error('Method not implemented.');
  }

  private isUserLoggedIn: boolean = false;
  loginStatus: any;
  addUser: any;

  private emailIdSource = new BehaviorSubject<string>('');
  currentEmailId = this.emailIdSource.asObservable();

  private filteredProductsSource = new BehaviorSubject<any[]>([]);
  currentFilteredProducts = this.filteredProductsSource.asObservable();


  constructor(private http: HttpClient) {
    this.isUserLoggedIn = false;
    this.loginStatus = new Subject();
    this.cartItems = [];

  }

  cartItemsChanged: EventEmitter<void> = new EventEmitter<void>();

  setFilteredProducts(filteredProducts: any[]) {
    this.filteredProductsSource.next(filteredProducts);
  }
  addToCart(product: any) {
    this.cartItems.push(product);
    this.productAddedToCart.next();
    this.cartItemsChanged.emit();

  }
  removeFromCart(pId: number) {
    const indexToRemove = this.cartItems.findIndex(item => item.pId === pId);
    if (indexToRemove !== -1) {
      this.cartItems.splice(indexToRemove, 1);
      this['saveCartItems']();
      this.cartItemsChanged.emit();
    }
  }

  getCartItems(): any[] {
    return [...this.cartItems];
  }

  setCartItems(items: any[]) {
    this.cartItems = items;
  }

  setIsUserLoggedIn() {
    this.isUserLoggedIn = true;
    this.loginStatus.next(true);
  }

  isLoggedIn(): boolean {
    return this.getIsUserLogged();
  }

  getIsUserLogged(): boolean {
    return this.isUserLoggedIn;
  }

  getLoginStatus(): any {
    return this.loginStatus.asObservable();
  }

  setIsUserLoggedOut() {
    this.isUserLoggedIn = false;
    this.loginStatus.next(false);
  }

  userLogin(emailId: any, password: any): any {
    return this.http.get('http://localhost:8080/loginUser/' + emailId + '/' + password).toPromise();
  }

  loginOwner(emailId: any, password: any): any {
    return this.http.get('http://localhost:8080/ownerLogin/' + emailId + '/' + password).toPromise();
  }

  loginNursery(emailId: any, password: any): any {
    return this.http.get('http://localhost:8080/nurseryLogin/' + emailId + '/' + password).toPromise();
  }

  registerUser(user: any): any {
    return this.http.post('http://localhost:8080/addUser', user);
  }

  registerOwner(owner: any): any {
    return this.http.post('http://localhost:8080/addOwner', owner);
  }

  registerNursery(nursery: any): any {
    return this.http.post('http://localhost:8080/addNursery', nursery);
  }


  updateUser(user: any): any {
    return this.http.put('http://localhost:8080/updateUser', user);
  }

  deleteNursery(nurseryId: any): any {
    return this.http.delete('http://localhost:8080/deleteNurseryById/' + nurseryId);
  }

  updateNursery(nursery: any): any {
    return this.http.put('http://localhost:8080/updateNursery', nursery);
  }

  getAllNurseries(): Observable<any> {
    return this.http.get<any>('http://localhost:8080/getAllNurseries');
  }

  getOwners(): Observable<any[]> {
    return this.http.get<any[]>('http://localhost:8080/getOwners');
  }

  updateOwner(owner: any): any {
    return this.http.put('http://localhost:8080/updateOwner', owner);
  }

  deleteOwner(ownerId: any): any {
    return this.http.delete('http://localhost:8080/deleteOwnerById/' + ownerId);
  }

  getUsers(): Observable<any[]> {
    return this.http.get<any[]>('http://localhost:8080/getUsers');
  }

  updateUsers(user: any): any {
    return this.http.put('http://localhost:8080/updateUser', user);
  }

  deleteUser(userId: any): any {
    return this.http.delete('http://localhost:8080/deleteUserById/' + userId);
  }

  deleteProduct(pId: any): any {
    return this.http.delete('http://localhost:8080/deleteProductById/' + pId);
  }
  changeEmailId(emailId: string) {
    this.emailIdSource.next(emailId);
  }

  updateUserPassword(user: any): any {
    return this.http.put('http://localhost:8080/updateUserPassword', user);
  }

  getUserOtp(emailId: any): any {
    return this.http.get('http://localhost:8080/getUserEmailId/' + emailId);
  }

  getProductByName(productName: string): Observable<any> {
    return this.http.get('http://localhost:8080/getProductByName/' + productName);
  }

  getFaqs(): Observable<any[]> {
    return this.http.get<any[]>('http://localhost:8080/getFaqs');
  }

  getNurseryByName(nurseryName: string): any {
    return this.http.get('http://localhost:8080/getNurseryByName/' + nurseryName);
  }

  addProduct(product: FormData): Observable<any> {
    return this.http.post('http://localhost:8080/addProduct', product);
  }

  updateProduct(product: FormData): Observable<any> {
    return this.http.put('http://localhost:8080/updateProduct', product);
  }

  getProducts(): Observable<any[]> {
    return this.http.get<any[]>('http://localhost:8080/getProducts');
  }

  getProductByPlantCategory(plant: string): Observable<any[]> {
    return this.http.get<any[]>('http://localhost:8080/getProductByPlantCategory/' + plant);
  }

  getProductByPotCategory(pot: string): Observable<any[]> {
    return this.http.get<any[]>('http://localhost:8080/getProductByPotCategory/' + pot);
  }

  getProductBySoilCategory(soil: string): Observable<any[]> {
    return this.http.get<any[]>('http://localhost:8080/getProductBySoilCategory/' + soil);
  }

  getProductByFertilizerCategory(fertilizers: string): Observable<any[]> {
    return this.http.get<any[]>('http://localhost:8080/getProductBySoilCategory/' + fertilizers);
  }

  getProductByPesticideCategory(pesticide: string): Observable<any[]> {
    return this.http.get<any[]>('http://localhost:8080/getProductByPesticideCategory/' + pesticide);
  }

  getProductBySeedCategory(seed: string): Observable<any[]> {
    return this.http.get<any[]>('http://localhost:8080/getProductBySeedCategory/' + seed);
  }

  getProductByToolCategory(tool: string): Observable<any[]> {
    return this.http.get<any[]>('http://localhost:8080/getProductByToolCategory/' + tool);
  }

  getProductByNurseryId(nurseryId: number): Observable<any[]> {
    return this.http.get<any[]>('http://localhost:8080/getProductByNurseryId/' + nurseryId);
  }

}
