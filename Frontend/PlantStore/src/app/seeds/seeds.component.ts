import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-seeds',
  templateUrl: './seeds.component.html',
  styleUrls: ['./seeds.component.css']
})
export class SeedsComponent implements OnInit {
  seeds: any[] = [];

  constructor(private service: UserService) { }

  ngOnInit(): void {
    const seed: string = 'seed'; // Define your category here

    this.service.getProductBySeedCategory(seed).subscribe(
      (seeds: any[]) => {
        this.seeds = seeds;
      },
      error => {
        console.log('Error fetching seds:', error);
      }
    );
  }
}