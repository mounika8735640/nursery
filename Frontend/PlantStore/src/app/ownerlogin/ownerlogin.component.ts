import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../user.service';

declare var google: any;

@Component({
  selector: 'app-ownerlogin',
  templateUrl: './ownerlogin.component.html',
  styleUrls: ['./ownerlogin.component.css']
})
export class OwnerloginComponent implements OnInit {
  emailId: string = '';
  password: any;
  captchaResponse: string = '';

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private userService: UserService

  ) {

  }
  ngOnInit(): void {
    google.accounts.id.initialize({
      client_id: '364713573542-2eua08udek4c712gc9c5jfo2ohbsqema.apps.googleusercontent.com',
      callback: (resp: any) => this.handleLogin(resp)

    });

    google.accounts.id.renderButton(document.getElementById("google-btn"), {
      theme: 'filled_blue',
      size: 'large',
      shape: 'rectangle',
      width: 350,
    })

  }

  async loginSubmit(loginForm: any) {
    this.emailId = loginForm.value.email;
    this.password = loginForm.value.password;
    console.log(this.emailId, this.password);
    this.userService.setIsUserLoggedIn();
    localStorage.setItem("emailId", this.emailId);
    this.router.navigate(['owners-navbar']);



    if (loginForm.invalid) {
      this.toastr.error('Please enter both email and password.', 'Error');
      return;
    }

  }
  handleCaptchaResponse(captchaResponse: string) {
    this.captchaResponse = captchaResponse;
  }



  private decodeToken(token: string) {
    return JSON.parse(atob(token.split(".")[1]));
  }

  handleLogin(response: any) {
    console.log("working");
    if (response) {
      const payLoad = this.decodeToken(response.credential)
      sessionStorage.setItem("loggedInUser", JSON.stringify(payLoad));
      localStorage.setItem("email", payLoad.email);
      this.userService.setIsUserLoggedIn();
      this.router.navigate(['owners-navbar']);
    }
  }
}




