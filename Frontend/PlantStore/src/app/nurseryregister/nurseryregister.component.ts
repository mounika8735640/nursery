import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
selector: 'app-nurseryregister',
templateUrl: './nurseryregister.component.html',
styleUrls: ['./nurseryregister.component.css']
})
export class NurseryregisterComponent implements OnInit {
nursery: any = {
  emailId: '',
  location: '',
  password: '',
  confirmPassword: '',
  phoneNumber: '',
  nurseryName: '',
  ownerId: '',
  remember: false
};
captchaResponse: string = '';

constructor(
  private service: UserService,
  private router: Router,
  private toastr: ToastrService
) {}

ngOnInit() {
  console.log('NurseryregisterComponent initialized');
}

submitRegistrationForm() {
  console.log('Registration Form submitted');
  console.log('Nursery Name:', this.nursery.nurseryName);
  console.log('Email Id:', this.nursery.emailId);
  console.log('Password:', this.nursery.password);
  console.log('Confirm Password:', this.nursery.confirmPassword);
  console.log('Location:', this.nursery.location);
  console.log('Phone Number:', this.nursery.phoneNumber);
  console.log('OwnerId:', this.nursery.ownerId);

  if (this.nursery.password !== this.nursery.confirmPassword) {
    console.log('Password and Confirm Password must be the same.');
    return;
  }


  this.service.registerNursery(this.nursery).subscribe(
    (data: any) => {
      console.log('Registration successful: ', data);
      this.toastr.success('Registration successful', 'Success');
      console.log('Navigating to NurseryLogin...');
      this.router.navigate(['nurserylogin']);
      console.log('Navigation completed.');
    },
    (error: any) => {
      this.toastr.error('Registration Failed', 'Error');
      console.error('Registration failed: ', error);
    }
  );
}

// handleCaptchaResponse(captchaResponse: string) {
//   this.captchaResponse = captchaResponse;
// }
}