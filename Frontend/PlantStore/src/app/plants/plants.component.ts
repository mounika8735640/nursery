import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-plants',
  templateUrl: './plants.component.html',
  styleUrls: ['./plants.component.css']
})
export class PlantsComponent implements OnInit {
  plants: any[] = [];

  constructor(private service: UserService) { }

  ngOnInit(): void {
    // Assuming 'plant' is a string representing a category
    const plant: string = 'plant'; // Define your category here

    this.service.getProductByPlantCategory(plant).subscribe(
      (plants: any[]) => {
        this.plants = plants;
      },
      error => {
        console.log('Error fetching plants:', error);
      }
    );
  }
  
}
