import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-userheader',
  templateUrl: './userheader.component.html',
  styleUrls: ['./userheader.component.css']
})
export class UserheaderComponent implements OnInit {
  activeSlideIndex = 0;
  slides = [
    { image: './assets/C1.webp' },
    { image: './assets/C2.webp' },
    { image: './assets/C4.webp' },
    { image: './assets/C5.webp' },
  ];
  loginStatus: any;
  searchQuery: string = '';
  cartItemCount: number = 0;
  cartItems: any;

  constructor(private service: UserService, private toastr: ToastrService) { }

  ngOnInit() {
    this.service.cartItemsChanged.subscribe(() => {
      this.updateCartItemCount();
    });
    this.service.getLoginStatus().subscribe((data: any) => {
      this.loginStatus = data;
      this.updateCartItemCount();

      this.service.productAddedToCart.subscribe(() => {
        this.updateCartItemCount();

      });



    });
  }
  prevSlide() {
    this.activeSlideIndex = (this.activeSlideIndex === 0) ? this.slides.length - 1 : this.activeSlideIndex - 1;
  }

  nextSlide() {
    this.activeSlideIndex = (this.activeSlideIndex === this.slides.length - 1) ? 0 : this.activeSlideIndex + 1;
  }

  search() {
    if (this.searchQuery.trim() !== '') {
      console.log('Performing search for:', this.searchQuery);
      this.service.getProductByName(this.searchQuery).subscribe((results: any[]) => {
        if (results && results.length === 0) {
          this.toastr.error('No matching results found!', 'Error');
        } else {
          console.log('Search results:', results);
        }
      });

      // Assuming getNurseryByName is a similar method for nursery search
      // Uncomment this block if getNurseryByName method exists
      /*
      this.userService.getNurseryByName(this.searchQuery).subscribe((results: any[]) => {
        if (results && results.length === 0) {
          this.toastr.error('No matching nursery found!', 'Error');
        } else {
          console.log('Search results:', results);
        }
      });
      */
    }
  }

  updateCartItemCount() {
    // Get cart items count from UserService
    const cartItems = this.service.getCartItems();

    this.cartItemCount = cartItems.length;
  }

}
