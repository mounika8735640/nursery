import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-tools',
  templateUrl: './tools.component.html',
  styleUrls: ['./tools.component.css']
})
export class ToolsComponent implements OnInit {
  tools: any[] = [];

  constructor(private service: UserService) { }

  ngOnInit(): void {
    // Assuming 'plant' is a string representing a category
    const tool: string = 'tool'; // Define your category here

    this.service.getProductByToolCategory(tool).subscribe(
      (tools: any[]) => {
        this.tools = tools;
      },
      error => {
        console.log('Error fetching seds:', error);
      }
    );
  }
}