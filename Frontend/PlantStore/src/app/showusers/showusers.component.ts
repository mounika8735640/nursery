import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';


@Component({
  selector: 'app-showusers',
  templateUrl: './showusers.component.html',
  styleUrl: './showusers.component.css'
})
export class ShowusersComponent implements OnInit{
  users: any;
  emailId: any;

  constructor(private service: UserService) {
    this.emailId = localStorage.getItem('emailId');
   
  }

  ngOnInit() {
    this.service.getUsers().subscribe((data: any) => {
      this.users = data;
    });
  }


}
