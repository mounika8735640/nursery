declare var Razorpay: any;

import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  products: any[] = [];
  total: number = 0;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.loadCartItems();
  }

  loadCartItems() {
    this.products = this.userService.getCartItems();
    this.calculateTotal();
  }

  calculateTotal() {
    this.total = this.products.reduce((sum, product) => sum + product.price, 0);
  }

  deleteCartProduct(product: any) {
    const index = this.products.findIndex(p => p.id === product.id);
    if (index !== -1) {
      this.products.splice(index, 1);
      this.calculateTotal();
    }
  }

  purchase() {
    this.userService.setCartItems([]);
    this.products = [];
    this.total = 0;
  }
  payNow(){
    if (this.total <= 0) {
      console.error("Total amount should be greater than 0");
      return;
    }
    const RazorpayOptions = {
      description :'Sample Rozorpay demo',
      currency :'INR',
      amount: this.total * 100, 
      name : 'Mounika',
      key : 'rzp_test_chEE74MxFAuQwE',
      image:'https://i.imur.com/FApqk3D,jpeg',
      prefill :{
        name:'Mounika',
        email:'mounikamouni8311@gmail.com',
        phone :'9390567983'
      },
      theme:{
        color:'#f37254'
      },
      modal :{
        ondismiss :()=>{
          console.log('dismissed');
        }
      }

    }
    const successCallback = (paymentid: any) => {
      console.log(paymentid);
      this.purchase();
    };
    const failureCallback=(e:any)=>{
      console.log(e);

    }
    Razorpay.open(RazorpayOptions,successCallback,failureCallback)

  }
  
}
