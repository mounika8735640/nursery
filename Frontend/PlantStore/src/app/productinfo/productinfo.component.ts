import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../user.service';

declare var Razorpay: any;

@Component({
  selector: 'app-productinfo',
  templateUrl: './productinfo.component.html',
  styleUrls: ['./productinfo.component.css']
})
export class ProductinfoComponent implements OnInit {
  selectedProduct: any;
  loading: boolean = true;
  error: string | null = null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: UserService
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const productName = params['productName'];
      console.log('Product Name:', productName);

      if (productName !== undefined && productName !== '') {
        this.service.getProductByName(productName).subscribe(
          (product: any) => {
            if (product) {
              this.selectedProduct = product;
              this.loading = false;
            } else {
              this.error = 'Product not found';
              this.loading = false;
            }
          },
          (error: any) => {
            console.log('Error fetching the Product data: ', error);
            this.error = 'Error fetching product information';
            this.loading = false;
          }
        );
      } else {
        console.log('Invalid product name');
        this.error = 'Invalid product name';
        this.loading = false;
      }
    });
  }

  addToCart(product: any) {
    if (this.service.isLoggedIn()) {
      console.log('Product added to cart:', product);
      this.service.addToCart(product);
      const storedCartItems = localStorage.getItem('cartItems') || '[]';
      const cartItems = JSON.parse(storedCartItems);
      cartItems.push(product);
      localStorage.setItem('cartItems', JSON.stringify(cartItems));

      this.router.navigate(['/cart']);
    } else {
      alert('Please log in first to add items to your cart.');
      this.router.navigate(['/login1']);
    }
  }

  payNow() {
    if (this.service.isLoggedIn()) {
      const RazorpayOptions = {
        description: 'Sample Razorpay demo',
        currency: 'INR',
        amount: this.selectedProduct.price * 100, // Assuming the product price is in rupees
        name: 'Mounika',
        key: 'rzp_test_chEE74MxFAuQwE',
        image: 'https://i.imur.com/FApqk3D,jpeg',
        prefill: {
          name: 'Mounika',
          email: 'mounikamouni8311@gmail.com',
          phone: '9390567983'
        },
        theme: {
          color: '#f37254'
        },
        modal: {
          ondismiss: () => {
            console.log('dismissed');
          }
        }
      };

      const successCallback = (paymentid: any) => {
        console.log(paymentid);
        // Additional logic on successful payment if needed
      };

      const failureCallback = (e: any) => {
        console.log(e);
        // Additional logic on payment failure if needed
      };

      Razorpay.open(RazorpayOptions, successCallback, failureCallback);
    } else {
      alert('Please log in first to proceed with the payment.');
      this.router.navigate(['/login1']);
    }
  }
}
